
HASPOC-binaries
===============

This repository contains pre-built boot binaries and various other components such as board firmware needed to build and upload HASPOC software into the HiKey board.


This repository will be included in the hypervisor project if build TARGET is hikey. You normally do not need to perform any additional actions.
