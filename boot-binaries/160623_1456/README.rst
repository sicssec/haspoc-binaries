
This directory contains ARM Trusted Firmware and Secure Boot binaries for HiKey in addition to some firmware binaries provided by HiSilicon.



ARM Trusted Firmware (ATF)
--------------------------


The files include

* fip.bin, stage 2 boot
* bl31.bin, boot payload patched for Secure Boot

For more information, see https://github.com/96boards/arm-trusted-firmware


Secure Boot
-----------

The files include

* bl1.bin, stage 1 boot
* hikey4.ini, boot manifest and signing information



HiSilicon
----------

The files include

* flashloader.bin, to be used with ATF. patched for Secure Boot
* l-loader.bin, to be used with ATF
* mcuimage.bin, bl30 firmware for the supervisor core
* ptable-linux.img, a ptable extended to have room for large payloads (i.e. linux + ramfs)




Notes
~~~~~

Some files have been modified for use with Secure Boot. This is mostly limited to modified load address and boot parameters.
